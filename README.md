Фронтенд-часть приложения Starosta.
Приложение для преподавателей и студентов ВУЗов. 
Необходимо сделать регистрацию преподавателей, студентов, 
расписание и таблицу с отметками. 
Основано на идее: 
https://github.com/codedokode/pasta/blob/master/js/spa.md

Доска с задачами по разработке в Trello:
https://trello.com/b/nQO9Z2jv

# starosta

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test-watch
```

### Lints and fixes files
```
npm run lint
```
