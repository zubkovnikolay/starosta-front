import Vue from 'vue'
import Vuetify from 'vuetify'
import Vuex from 'vuex'
import helpers from './src/helpers/helpers'
import filters from './src/filters/filters'

Vue.use(Vuetify)
Vue.use(Vuex)
Vue.mixin(helpers)
for(const index in filters.filters) {
  Vue.filter(index, filters.filters[index])
}