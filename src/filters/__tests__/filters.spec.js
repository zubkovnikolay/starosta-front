import { mount, createLocalVue } from '@vue/test-utils'
import filters from '../filters'
import { STUDY_YEAR_DATE } from '../../utils/calendar/calendar'
import Test from '../../components/Test'
const localVue = createLocalVue()

describe('filters', () => {
  it('filters day, year, month, day&month, date of full date', () => {
    const currentDate = new Date(STUDY_YEAR_DATE)
    const wrapper = mount(Test, {
      filters: filters.filters,
      propsData: {
        currentDate
      },
      localVue
    })
    const day = wrapper.find('.day').text()
    const month = wrapper.find('.month').text()
    const year = wrapper.find('.year').text()
    const dayAndMonth = wrapper.find('.day_and_month').text()
    const date = wrapper.find('.date').text()
    
    expect(day).toEqual('02')
    expect(month).toEqual('09')
    expect(year).toEqual('2019')
    expect(dayAndMonth).toEqual('02.09')
    expect(date).toEqual('02.09.2019')
  })
})
