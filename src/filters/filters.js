export default {
  filters: {
    date: function (fullDate) {
      if(Object.prototype.toString.call(fullDate) === '[object Date]') {
        return `${("0" + fullDate.getDate()).slice(-2)}.${("0" + (fullDate.getMonth() + 1)).slice(-2)}.${fullDate.getFullYear()}`
      }
      return fullDate.slice(3, -5)
    },
    dayAndMonth: function (fullDate) {
      if(Object.prototype.toString.call(fullDate) === '[object Date]') {
        return `${("0" + fullDate.getDate()).slice(-2)}.${("0" + (fullDate.getMonth() + 1)).slice(-2)}`
      }
      return fullDate.slice(3, -5)
    },
    month: function (fullDate) {
      if(Object.prototype.toString.call(fullDate) === '[object Date]') {
        return `${("0" + (fullDate.getMonth() + 1)).slice(-2)}`
      }
      return fullDate.slice(3, -5)
    },
    day: function (fullDate) {
      if(Object.prototype.toString.call(fullDate) === '[object Date]') {
        return `${("0" + fullDate.getDate()).slice(-2)}`
      }
      return fullDate.slice(0, 2)
    },
    year: function (fullDate) {
      if(Object.prototype.toString.call(fullDate) === '[object Date]') {
        return `${fullDate.getFullYear()}`
      }
      return fullDate.slice(-4)
    },
  }
}