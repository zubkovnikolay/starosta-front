import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import 'vuetify/dist/vuetify.min.css'
import { sync } from 'vuex-router-sync'
import filters from './filters/filters'
import helpers from './helpers/helpers'

import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/dist/css/swiper.css'


Vue.use(VueAwesomeSwiper)

sync(store, router)

Vue.mixin(helpers)

Vue.config.productionTip = false
for(const index in filters.filters) {
  Vue.filter(index, filters.filters[index])
}

new Vue({
  vuetify,
  router,
  store,
  render: h => h(App),
}).$mount('#app')
