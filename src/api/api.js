export const ROOT_API = 'http://starosta/backend'

export const schedule = (groupId, weekId) => `${ROOT_API}/schedule/group/${groupId}/week/${weekId}`