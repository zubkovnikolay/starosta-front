import colors from "vuetify/lib/util/colors"
import '@mdi/font/css/materialdesignicons.css'
export default {
  theme: {
    themes: {
      light: {
        primary: colors.green.darken1, // #E53935
        secondary: colors.red.lighten4, // #FFCDD2
        accent: colors.green.base, // #3F51B5
      },
    },
  },
  iconfont: 'mdiSvg'
}