import Vue from 'vue'
import Vuetify from 'vuetify'
import vuetifyConfig from './vuetify-config'

Vue.use(Vuetify)

export default new Vuetify(vuetifyConfig)