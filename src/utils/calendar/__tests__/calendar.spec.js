import { mount, createLocalVue } from '@vue/test-utils'
import { calendar, STUDY_WEEKS_QUANTITY, STUDY_YEAR_DATE } from '../calendar'
const localVue = createLocalVue()
const mountFunc = options => mount({
    render() {},
    mixins: [calendar]
  }, Object.assign({}, {
    propsData: {
      startDate: new Date(STUDY_YEAR_DATE)
    },
  localVue
  }, options)
)
describe('calendar', () => {
  it('returns a set of days', () => {
    const wrapper = mountFunc()
    const days = wrapper.vm.weeksMap[1]
    expect(Array.isArray(days)).toBe(true)
    expect(days.length === 7).toBe(true)
  })
  
  it('creates a dates map', () => {
    const wrapper = mountFunc()
    expect(wrapper.vm.datesMap[wrapper.vm.date]).toEqual(1)
  })
  
  it('returns a week days of a week number', () => {
    const wrapper = mountFunc()
    expect(wrapper.vm.getDaysByWeekNumber(1).length).toEqual(7)
  })
  
  it('defines number of current week', () => {
    const wrapper = mountFunc()
    expect(wrapper.vm.getWeekNumberByDate(wrapper.vm.date)).toEqual(1)
  })
  
  it('defines day name by date', () => {
    const wrapper = mountFunc()
    expect(wrapper.vm.getDayNameByDate(wrapper.vm.date)).toContain('понедельник')
  })
  
  it('gives day, year, month, date, monthName', () => {
    const vm = mountFunc().vm
    const {day, date, year, month, monthName} = vm
    expect(day).toContain('понедельник')
    expect(date).toEqual("02.09.2019")
    expect(year).toEqual("2019")
    expect(month).toEqual("09")
    expect(monthName).toContain("сентябрь")
  })
  it('returns weeks numbers', () => {
    const wrapper = mountFunc()
    expect(wrapper.vm.weeks.length).toEqual(STUDY_WEEKS_QUANTITY)
  })
})