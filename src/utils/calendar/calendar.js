export const STUDY_YEAR_DATE = "09.02.2019" //MM.DD.YYYY
export const STUDY_WEEKS_QUANTITY = 43
export const STUDY_DAYS_IN_WEEK = 6
export const DAY_NAMES = {
  1: 'понедельник',
  2: 'вторник',
  3: 'среда',
  4: 'четверг',
  5: 'пятница',
  6: 'суббота',
  0: 'воскресенье'
}
export const MONTH_NAMES = {
  0: "январь",
  1: "февраль",
  2: "март",
  3: "апрель",
  4: "май",
  5: "июнь",
  6: "июль",
  7: "август",
  8: "сентябрь",
  9: "октябрь",
  10: "ноябрь",
  11: "декабрь"
}
export const calendar =  {
  name: 'calendar',
  props: {
    startDate: {
      type: Date,
      default: () => (new Date())
    }
  },
  data() {
    return {
      currentDate: this.startDate,
      studyDate: new Date(STUDY_YEAR_DATE),
      weeksMap: {},
      datesMap: {},
      daysMap: {},
    }
  },
  computed: {
    date() {
      return this.formatDate(this.currentDate)
    },
    day() {
      return this.daysMap[this.date]
    },
    month() {
      return this.formatMonth(this.currentDate)
    },
    monthName() {
      return MONTH_NAMES[this.currentDate.getMonth()]
    },
    year() {
      return `${this.currentDate.getFullYear()}`
    },
    weeks() {
      return Object.keys(this.weeksMap)
    }
  },
  created() {
    this.createWeeksAndDaysMaps()
  },
  methods: {
    createWeeksAndDaysMaps() {
      let cursorDate = this.studyDate
      let weeks = {}
      let dates = {}
      let days = {}
      for (let weekCounter = 1; weekCounter <= STUDY_WEEKS_QUANTITY; weekCounter++) {
        let datesPack = []
        for (let dayCounter = 1; dayCounter <= 7; dayCounter++) {
          let formattedDate = this.formatDate(cursorDate)
          datesPack.push(formattedDate)
          dates[formattedDate] = weekCounter
          days[formattedDate] = DAY_NAMES[cursorDate.getDay()]
          cursorDate.setDate(cursorDate.getDate() + 1)
        }
        weeks[weekCounter] = datesPack
      }
      this.weeksMap = Object.assign({}, this.weeksMap, weeks)
      this.datesMap = Object.assign({}, this.datesMap, dates)
      this.daysMap = Object.assign({}, this.daysMap, days)
    },
    formatDate(date) {
      return `${this.formatDay(date)}.${this.formatMonth(date)}.${this.formatYear(date)}`
    },
    formatDay(date) {
      return ("0" + date.getDate()).slice(-2)
    },
    formatMonth(date) {
      return ("0" + (date.getMonth() + 1)).slice(-2)
    },
    formatDayAndMonth(date) {
      return this.formatDay(date) + '.' + this.formatMonth(date)
    },
    formatYear(date) {
      return date.getFullYear()
    },
    getDaysByWeekNumber(weekNumber) {
      return this.weeksMap[weekNumber]
    },
    getWeekNumberByDate(date) {
      return this.datesMap[date]
    },
    getDayNameByDate(date) {
      return this.daysMap[date]
    }
  }
}