export default {
  methods: {
    isDate(date) {
      return Object.prototype.toString.call(date) === '[object Date]'
    }
  }
}