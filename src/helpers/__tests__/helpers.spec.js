import helpers from '../helpers'

describe('helpers', () => {
  it('checks whether the object is Date', () => {
    expect(helpers.methods.isDate(new Date())).toBe(true)
  })
})