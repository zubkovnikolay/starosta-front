export default {
  data() {
    return {
      modalModel: false
    }
  },
  computed: {
    isOpen() {
      return this.modalModel === true
    }
  },
  methods: {
    open() {
      this.modalModel = true
    },
    close() {
      this.modalModel = false
    }
  }
}