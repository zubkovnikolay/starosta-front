import { mount, createLocalVue } from '@vue/test-utils'
import Vuetify from 'vuetify'
import store from '../../../store'
import router from '../../../router'
import { sync } from 'vuex-router-sync'
import SelectRing from '../SelectRing'

const localVue = createLocalVue()
sync(store, router)

describe('SelectRing', () => {
  let vuetify

  beforeEach(() => vuetify = new Vuetify())

  const mountFunction = options => mount(SelectRing, {
    localVue,
    store,
    router,
    vuetify,
    ...options
  })
  it('changes id', () => {
    const items = Array.from(Array(43), (_, index) => ++index)
    const itemName = 'week'

    const wrapper = mountFunction({propsData: {items, itemName}})
    let id = wrapper.vm.id

    let weeks = wrapper.findAll(`.${itemName}`)

    weeks.at(3).trigger('click')

    expect(wrapper.vm.id !== id).toBe(true)
    expect(wrapper.emitted('selected')).toBeTruthy()
  })
})