import { mount, createLocalVue } from '@vue/test-utils'
import Vuetify from 'vuetify'
import store from '../../store'
import router from '../../router'
import { sync } from 'vuex-router-sync'
import Login from '../Login/Login'

const localVue = createLocalVue()

sync(store, router)

describe('Login', () => {
  let vuetify
  
  beforeEach(() => vuetify = new Vuetify())
  
  const mountFunction = options => mount(Login, {
    localVue,
    vuetify,
    router,
    store,
    ...options,
  })
  
  it('renders and calls login form on open() method call', () => {
    const wrapper = mountFunction()
    wrapper.vm.open()
    expect(wrapper.html()).toMatchSnapshot()
    expect(wrapper.vm.isOpen).toBe(true)
  })
  
  it('contains login and password fields', () => {
    const wrapper = mountFunction()
    wrapper.vm.open()
    expect(wrapper.find('#login').exists()).toBe(true)
    expect(wrapper.find('#password').exists()).toBe(true)
  })
  
  it('validates fields and displays messages if fields\' values are not valid', () => {
    const wrapper = mountFunction()
    wrapper.vm.open()
    expect(wrapper.vm.isLoginValid('test@mail.com')).toBe(true)
    expect(wrapper.vm.isPasswordValid('ab3CD12')).toBe(true)
    
    wrapper.find('#login').setValue('7777')
    wrapper.find('#password').setValue('ssds')
    
    wrapper.vm.$refs.form.validate()
    
    expect(wrapper.findAll('.v-messages__message').length).toBe(2)
  })
  
  it('able submit only if all required fields are valid', () => {
    const wrapper = mountFunction()
    wrapper.vm.open()
    const onSubmit = jest.fn()
    
    wrapper.setMethods({
      onSubmit: onSubmit
    })
    
    const user = {
      login: 'test@mail.com',
      password: '123456'
    }
    
    wrapper.find('#login').setValue(user.login)
    wrapper.find('#password').setValue(user.password)
    expect(wrapper.vm.$refs.form.validate()).toBe(true)
    
    wrapper.find('#submit').trigger('click')
    
    expect(onSubmit).toHaveBeenCalled()
  })
})