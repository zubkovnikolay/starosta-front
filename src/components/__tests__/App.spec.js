import { mount, createLocalVue } from '@vue/test-utils'
import Vuetify from 'vuetify'
import store from '../../store'
import router from '../../router'
import { sync } from 'vuex-router-sync'
import App from '../../App'
import Schedule from '../../components/Schedule/Schedule'

const localVue = createLocalVue()

sync(store, router)

describe('App', () => {
  let vuetify
  
  beforeEach(() => vuetify = new Vuetify())
  
  const mountFunction = options => mount(App, {
    localVue,
    vuetify,
    router,
    store,
    ...options,
  })
  
  it('renders', () => {
    const wrapper = mountFunction()
    expect(wrapper.html()).toMatchSnapshot()
    expect(wrapper.html()).toContain('Зарегистрироваться')
  })
  
  it('hides user menu if user is not authenticated', () => {
    const wrapper = mountFunction()
    expect(wrapper.vm.links.length === 0).toBe(false)
  })
  
  it('shows user menu hides login link if user is authenticated', () => {
    const computed = {
      isUserLoggedIn: jest.fn(() => true)
    }
    const wrapper = mountFunction({ computed })
    expect(wrapper.find('i.mdi-lock').exists()).toBe(false)
    expect(wrapper.vm.links.length > 0).toBe(true)
  })
  
  it('changes content relying on route', () => {
    const computed = {
      isUserLoggedIn: jest.fn(() => true)
    }
    const wrapper = mountFunction({ computed })
    
    wrapper.find('a[href="/schedule"]').trigger('click')
    
    expect(wrapper.find(Schedule).exists()).toBe(true)
  })
})