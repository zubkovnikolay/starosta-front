import {mount, createLocalVue} from '@vue/test-utils'
import Vuetify from 'vuetify'
import Lesson from '../Schedule/Lesson'
import router from '../../router'
import store from '../../store'

const localVue = createLocalVue()

describe('Lesson', () => {
  let vuetify
  
  beforeEach(() => vuetify = new Vuetify())
  
  const mountFunction = options => mount(Lesson, {
    localVue,
    vuetify,
    router,
    store,
    ...options,
  })
  
  it('renders', () => {
    const lessonData = {
      date: "02.09.2019",
      name: "КультуроФизкультура",
      type: "лекция",
      teacherName: "Заявъялова Наталья Нарутовна",
      auditory: "405"
    }
    const wrapper = mountFunction({propsData: {lessonData}})
    
    expect(wrapper.html()).toMatchSnapshot()
  })
  
  it('displays lesson data', () => {
    const wrapper = mountFunction()
    expect(wrapper.html()).toContain('КультуроФизкультура')
  })
})