import { mount, createLocalVue } from '@vue/test-utils'
import Vuetify from 'vuetify'
import store from '../../store'
import router from '../../router'
import { sync } from 'vuex-router-sync'
import Schedule from '../Schedule/Schedule'

const localVue = createLocalVue()

sync(store, router)

describe('Schedule', () => {
  let vuetify
  
  beforeEach(() => vuetify = new Vuetify())
  
  const mountFunction = options => mount(Schedule, {
    localVue,
    vuetify,
    router,
    store,
    ...options,
  })
  
  it('renders', () => {
    const wrapper = mountFunction()
    
    expect(wrapper.html()).toMatchSnapshot()
  })
  
  it('calls schedule with API when component had mounted', () => {
    const methods = {
      getSchedule: jest.fn()
    }
    
    mountFunction({ methods })
    
    expect(methods.getSchedule).toHaveBeenCalled()
  })

  it('draws a grid', () => {
    const wrapper = mountFunction()

    const cols = wrapper.findAll('.day-col')

    expect(cols.length >= 6).toBe(true)
  })
})