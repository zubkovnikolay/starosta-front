import Home from '../components/Home'
import Test from '../components/Test'
import Login from '../components/Login/Login'
import Schedule from '../components/Schedule/Schedule'
import authGuard from './auth-guard'

export default [
  {
    name: 'home',
    path: '/',
    component: Home,
  },
  {
    name: 'login',
    path: '/login',
    component: Login,
    props: { open: true }
  },
  {
    name: 'registration',
    path: '/registration',
    component: Test,
  },
  {
    name: 'admin',
    path: '/admin',
    beforeEnter: authGuard,
    component: Test,
    children: [
      {
        name: 'requests',
        path: '/requests',
        component: Test
      }
    ],
  },
  {
    name: 'schedule',
    path: '/schedule',
    component: Schedule,
  },
]