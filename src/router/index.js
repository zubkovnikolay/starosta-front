import Vue from 'vue'
import VueRouter from 'vue-router'
import routerConfig from './router-config.js'

Vue.use(VueRouter)

export default new VueRouter(routerConfig)