import routes from './routes'

export default {
  mode: 'history',
  base: '/',
  routes
}