import VueRouter from 'vue-router'
import Vuex from 'vuex'
import routerConfig from '../router-config'
import { createLocalVue } from '@vue/test-utils'

const localVue = createLocalVue()
localVue.use(VueRouter)
localVue.use(Vuex)

describe('router', () => {
  it('renders a schedule component via routing', () => {
    const router = new VueRouter(routerConfig)
    router.push('/schedule').catch(error => {
      throw error
    })
    expect(router.currentRoute.path).toBe(`/schedule`)
  })
  
  it('guards route before walking with it', () => {
    const router = new VueRouter(routerConfig)
    router.push('/admin').catch(error => {
      throw error
    })
    expect(router.currentRoute.path).toBe(`/`)
  })
})



