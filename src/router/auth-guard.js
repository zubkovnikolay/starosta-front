import store from '../store'

let entryUrl = null

export default async (to, from, next) => {
  if (store.getters.auth.loggedIn) {
    if (entryUrl) {
      const url = entryUrl
      entryUrl = null
      return next(url)
    } else {
      next()
    }
  }
  await store.commit('setAuth', true)
  
  if(store.getters.auth.loggedIn) {
    next()
  } else {
    entryUrl = to.path
  }
}