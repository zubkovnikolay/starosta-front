import createPersistedState from 'vuex-persistedstate'
import common from './common'
import schedule from './schedule'
import user from './user'

export default {
  plugins: [
    createPersistedState({
      paths: ['user']
    })
  ],
  modules: {
    common,
    schedule,
    user
  }
}