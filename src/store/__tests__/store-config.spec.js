import { createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import storeConfig from '../store-config'
import cloneDeep from 'lodash.clonedeep'
import { schedule as scheduleData } from '../schedule'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('store-config', () => {
  it('get schedule from store', () => {
    const store = new Vuex.Store(cloneDeep(storeConfig))
    const schedule = store.getters.schedule
    expect(schedule['02.09.2019'] !== undefined).toBe(true)
  })
  
  it('sets schedule from store', () => {
    const items = scheduleData.state.schedule.lessons
    const store = new Vuex.Store(cloneDeep(storeConfig))
    store.commit('setSchedule', items)
    const schedule = store.getters.schedule
    expect(schedule).toBe(items)
  })
})