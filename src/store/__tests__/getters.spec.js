import schedule from '../schedule'

describe('getters', () => {
  it('get schedule from store', () => {
    expect(schedule.getters.schedule(schedule.state).hasOwnProperty('02.09.2019')).toBe(true)
    expect(schedule.getters.schedule(schedule.state)['02.09.2019'] !== undefined).toBe(true)
  })
})