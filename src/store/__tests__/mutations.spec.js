import schedule from '../schedule'
const state = {
  schedule: {
    lessons: []
  }
}
const items = schedule.state.schedule.lessons

describe('setters', () => {
  it('sets store state', () => {
    schedule.mutations.setSchedule(state, items)
    
    expect(state.schedule.lessons).toBe(items)
    expect(Object.keys(state.schedule.lessons['02.09.2019']).length > 0).toBe(true)
  })
})