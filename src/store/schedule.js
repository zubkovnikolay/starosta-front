import {schedule} from '../api/api.js'
import axios from 'axios'
//weeks как страницы: уроки к ним не привязаны, они нужны только для навигации, представляют OFFSET
//время начала пары это строки или id строк; дни - это столбцы, представляют LIMIT,
//уроки привязываются к дате, которая уже привязывается ко времени
// можно установить просмотр от одного до 12 дней
export default {
  state: {
    timeGrid: [
      '8:00',
      '9:30',
      '11:00',
      '12:40',
      '14:10',
      '15:40',
      '17:00',
    ],
    groups: [
      {
        id: 1,
        name: 'ABZ-11',
      },
      {
        id: 2,
        name: 'ABZ-12',
      },
      {
        id: 3,
        name: 'ABZ-13'
      }
    ],
    groupSelected: 1,
    ignoreGroups: [],
    weekSelected: 1,
    schedule: {
      lessons: {
        "02.09.2019": {
          "8:00": {
            "name": "Иностранный zyk",
            "type": "лекция",
            "teacherName": "Никифоров Любомир Олегович",
            "auditory": "116"
          },
          "9:30": {
            "name": "Психология",
            "type": "лекция",
            "teacherName": "Копылова Ясмина Николаевна",
            "auditory": "102"
          },
          "11:00": {
            "name": "Фарманекрология",
            "type": "семинар",
            "teacherName": "Носков Овидий Наумович",
            "auditory": "405"
          },
          "12:40": {
            "name": "Метаэкономика",
            "type": "семинар",
            "teacherName": "Авдеев Митрофан Лукьевич",
            "auditory": "218"
          },
          "14:10": {
            "name": "КультуроФизкультура",
            "type": "лекция",
            "teacherName": "Заявъялова Наталья Нарутовна",
            "auditory": "405"
          },
          "15:40": {},
          "17:00": {
            "name": "История ранневековья",
            "type": "лекция",
            "teacherName": "Юдин Виссарион Николаевич",
            "auditory": "332"
          }
        },
        "03.09.2019": {
          "8:00": {},
          "9:30": {
            "name": "КультуроФизкультура",
            "type": "лекция",
            "teacherName": "Заявъялова Наталья Нарутовна",
            "auditory": "405"
          },
          "11:00": {},
          "12:40": {
            "name": "Редкология",
            "type": "лекция",
            "teacherName": "Коновалов Абрам Степанович",
            "auditory": "651"
          },
          "14:10": {
            "name": "Метаэкономика",
            "type": "лекция",
            "teacherName": "Авдеев Митрофан Лукьевич",
            "auditory": "218"
          },
          "15:40": {},
          "17:00": {}
        },
        "04.09.2019": {
          "8:00": {},
          "9:30": {
            "name": "Физика",
            "type": "лекция",
            "teacherName": "Федотов Вениамин Станиславович",
            "auditory": "404"
          },
          "11:00": {
            "name": "Психология",
            "type": "лекция",
            "teacherName": "Копылова Ясмина Николаевна",
            "auditory": "102"
          },
          "12:40": {
            "name": "Психология",
            "type": "лекция",
            "teacherName": "Копылова Ясмина Николаевна",
            "auditory": "102"
          },
          "14:10": {
            "name": "История ранневековья",
            "type": "лекция",
            "teacherName": "Юдин Виссарион Николаевич",
            "auditory": "332"
          },
          "15:40": {
            "name": "Телепортация",
            "type": "лекция",
            "teacherName": "Тихонов Емельян Германнович",
            "auditory": "606"
          },
          "17:00": {
            "name": "КультуроФизкультура",
            "type": "лекция",
            "teacherName": "Заявъялова Наталья Нарутовна",
            "auditory": "405"
          }
        },
        "05.09.2019": {
          "8:00": {
            "name": "Метаэкономика",
            "type": "лекция",
            "teacherName": "Авдеев Митрофан Лукьевич",
            "auditory": "218"
          },
          "9:30": {
            "name": "Редкология",
            "type": "семинар",
            "teacherName": "Коновалов Абрам Степанович",
            "auditory": "651"
          },
          "11:00": {
            "name": "Телепортация",
            "type": "лекция",
            "teacherName": "Тихонов Емельян Германнович",
            "auditory": "606"
          },
          "12:40": {},
          "14:10": {
            "name": "История ранневековья",
            "type": "семинар",
            "teacherName": "Юдин Виссарион Николаевич",
            "auditory": "332"
          },
          "15:40": {
            "name": "Физика",
            "type": "лекция",
            "teacherName": "Федотов Вениамин Станиславович",
            "auditory": "404"
          },
          "17:00": {}
        },
        "06.09.2019": {
          "8:00": {
            "name": "Пиратское право",
            "type": "лекция",
            "teacherName": "Арррхипов Валентин Сталкерович",
            "auditory": "010"
          },
          "9:30": {
            "name": "Мифология",
            "type": "лекция",
            "teacherName": "Максимов Борис Созонович",
            "auditory": "404"
          },
          "11:00": {
            "name": "КультуроФизкультура",
            "type": "лекция",
            "teacherName": "Заявъялова Наталья Нарутовна",
            "auditory": "405"
          },
          "12:40": {
            "name": "Мифология",
            "type": "семинар",
            "teacherName": "Максимов Борис Созонович",
            "auditory": "404"
          },
          "14:10": {},
          "15:40": {
            "name": "Стипендия: выживание",
            "type": "лекция",
            "teacherName": "Самосадов Доширак Займович",
            "auditory": "332"
          },
          "17:00": {}
        },
        "07.09.2019": {
          "8:00": {
            "name": "Базовое искусство войны",
            "type": "лекция",
            "teacherName": "Герое-Третьева Алла Анимеевна",
            "auditory": "551"
          },
          "9:30": {
            "name": "КультуроФизкультура",
            "type": "лекция",
            "teacherName": "Заявъялова Наталья Нарутовна",
            "auditory": "405"
          },
          "11:00": {},
          "12:40": {
            "name": "Продвинутое искусство войны",
            "type": "лекция",
            "teacherName": "Герое-Третьева Алла Анимеевна",
            "auditory": "551"
          },
          "14:10": {
            "name": "Культура международного питья",
            "type": "лекция",
            "teacherName": "Дьячков Модест Проклович",
            "auditory": "100"
          },
          "15:40": {
            "name": "Экспертное искусство войны: палатки",
            "type": "семинар",
            "teacherName": "Герое-Третьева Алла Анимеевна",
            "auditory": "551"
          },
          "17:00": {}
        }
      }
    },
  },
  getters: {
    schedule(state) {
      return state.schedule.lessons || {}
    },
    groups(state) {
      return state.groups
    },
    ignoreGroups(state) {
      return state.ignoreGroups
    },
    groupSelected(state) {
      return state.groupSelected
    },
    weekSelected(state) {
      return state.weekSelected
    },
    timeGrid(state) {
      return state.timeGrid
    }
  },
  mutations: {
    setSchedule(state, lessons) {
      state.schedule.lessons = lessons
    },
    setGroups(state, groups) {
      state.groups = groups
    },
    setIgnoreGroup(state, ignoreGroups) {
      state.ignoreGroups = ignoreGroups
    },
    setGroupSelected(state, group) {
      state.groupSelected = group
    },
    setWeekSelected(state, weekId) {
      state.weekSelected = weekId
    },
    setTimeGrid(state, timeGrid) {
      state.timeGrid = timeGrid
    }
  },
  actions: {
      getSchedule: ({commit}, {groupId, weekId}) => axios.get(schedule(groupId, weekId))
        .then(({data}) => {
          commit('setSchedule', data)
        })
        .catch(error => {
          throw error
        })
  }
}