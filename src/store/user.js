export default {
  state: {
    auth: {
      loggedIn: false
    }
  },
  getters: {
    auth (state) {
      return state.auth
    },
    isUserLoggedIn(state) {
      return state.auth.loggedIn
    }
  },
  mutations: {
    setAuth (state, payload) {
      state.auth.loggedIn = payload
    },
  },
  actions: {
    async loginUser ({ commit }) {
      commit('clearError')
      commit('setLoading', true)
      try {
        commit('setAuth', true)
      } catch (error) {
        commit('setError', error.message)
      } finally {
        commit('setLoading', false)
      }
    },
    logoutUser({ commit }) {
      commit('setAuth', false)
    }
  }
}