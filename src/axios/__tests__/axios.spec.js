import axios from 'axios'
import { ROOT_API } from '../../api/api'
import flushPromises from 'flush-promises'

const url = `${ROOT_API}/schedule`

describe('axios', () => {
  it('GET query', async () => {
    expect.assertions(1)
    let response = {}
    
    await axios.get(url)
      .then(({data}) => (response = data))
      .catch(error => {
        throw error
      })
    
    await flushPromises()
    expect(response['02.09.2019']).toBeTruthy()
  })
})